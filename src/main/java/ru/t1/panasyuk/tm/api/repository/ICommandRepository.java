package ru.t1.panasyuk.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.panasyuk.tm.command.AbstractCommand;

import java.util.Collection;

public interface ICommandRepository {

    void add(@Nullable AbstractCommand command);

    @Nullable
    AbstractCommand getCommandByArgument(String argument);

    @Nullable
    AbstractCommand getCommandByName(String name);

    @NotNull
    Collection<AbstractCommand> getTerminalCommands();

}