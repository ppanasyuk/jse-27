package ru.t1.panasyuk.tm.command.data;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.panasyuk.tm.command.AbstractCommand;
import ru.t1.panasyuk.tm.dto.Domain;
import ru.t1.panasyuk.tm.enumerated.Role;

public abstract class AbstractDataCommand extends AbstractCommand {

    @NotNull
    public static final String FILE_BASE64 = "./data.base64";

    @NotNull
    public static final String FILE_BINARY = "./data.bin";

    public AbstractDataCommand() {
    }

    @NotNull
    public Domain getDomain() {
        @NotNull final Domain domain = new Domain();
        domain.setProjects(getServiceLocator().getProjectService().findAll());
        domain.setTasks(getServiceLocator().getTaskService().findAll());
        domain.setUsers(getServiceLocator().getUserService().findAll());
        return domain;
    }

    public void setDomain(@Nullable final Domain domain) {
        if (domain == null) return;
        getServiceLocator().getUserService().set(domain.getUsers());
        getServiceLocator().getProjectService().set(domain.getProjects());
        getServiceLocator().getTaskService().set(domain.getTasks());
        getServiceLocator().getAuthService().logout();
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

}
